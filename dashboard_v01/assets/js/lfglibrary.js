(function($)
{
	$.fn.lfglibrary = function(options)
	{
		var sl = null;
		this.each(function(e)
		{ 
			//initialize the class
			sl = new s();
		});
		return sl;
	}

	var s = (function()
		{
			function s(){
				//init function here
				this.evts();
			}
			s.prototype.evts = function(){
				var $this = this;
				$(".btn").on('click touchstart', function(e){   
					if($(this).hasClass("login")){ $this.login($(this));}
					if($(this).hasClass("signup")){ $this.signup($(this)); }
	            });
			}
			s.prototype.login = function(e)
			{
				var formid = "#login";
				var res = $.fget(formid, ".fn", "name");
				if(res.count == 0){
					var data = res.data;
					var server = $.getValues(data, $.path()+"auths/loginAuths", e, "POST");
					server.done(function(json){
						var j = $.parseJSON(json);
						
						if(j.s == 1)
						{
							$.status(formid, "Authentication successful.", "success", true);
							//add redirect...
							document.location.href = $.path()+"dashboard";
						}else{
							$.status(formid, "Authentication failed.", "danger", true);
						}

						
					});
					// server.fail(function(){ 
					// 	$.status(formid, "Authentication failed.", "warning", true);
					// })
					// //request auth
				}else{
					$.status(formid, "Can't Complete your request.", "danger", true);
				}

			}
			s.prototype.signup = function(e)
			{
				// var formid = "#signup";
				// var res = $.fget(formid, ".fn", "name");
				// if(res.count == 0){
				// 	var data = res.data;
				// 	var server = $.getValues(data, $.path()+"auths/signup", e, "POST");
				// 	server.done(function(json){
				// 		alert(json)
				// 		$.status(formid, "Authentication successful.", "success", true);
				// 	});
				// 	server.fail(function(){ 
				// 		$.status(formid, "Authentication failed.", "warning", true);
				// 	})
				// 	//request auth
				// }else{
				// 	$.status(formid, "Can't Complete your request.", "danger", true);
				// }
			}
			return s;
		})()
})(jQuery)

//Multi-Phase Ajax Request
jQuery.extend({
		alertx:".alertx",
		path:function()
		{
			var url = $("body").attr("id");			
			return 	url;
		},
		isNumeric:function(value){ return /^\d+$/.test(value);},
		status:function(formid, message, flag, timeout)
		{
			var timer = timeout ? 3000 : 0; 
			var html = '<div class="alert alert-'+flag+'" data-role="alert">'+message+'</div>';
			$(formid).find(".report").html(html);
			setTimeout(function(){
				$(formid).find(".report").html("");
			}, timer);
		},
		fget:function(formid,key,name)
		{
			var d={},f=$(formid).find(key), counter = 0;
			f.each(function()
			{
				//collect all value
				//hold reference to required count
				var k = $(this).attr(name), v = $(this).val();
				
				if($(this).val()!=""){
					d[k] = v;
				}
				if($(this).hasClass("required") && $(this).val()=="")
				{
					counter++;
				}
			});
			return {data:d,count:counter};
		},
		getValues:function(data,url_,dom,method)
		{
			var result=null;
			return $.ajax({url:url_,data:data,type:method,async:true,beforeSend:$.onSend});
		},
		onSend:function(){

		},
		onComplete:function(){ },
		parseJson:function(data){var jsonString=$.parseJSON(data);return jsonString;},
		notice:function(dom, msg){dom.html(msg).fadeIn("fast");setTimeout(function(){	dom.html(msg).fadeOut("fast");}, 5000);},
		getWidth:function(dom){return dom.width();},
		getHeight:function(dom){ return dom.height(); } 
	});

$(function(){ $("body").lfglibrary(); });