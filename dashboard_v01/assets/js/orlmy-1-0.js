(function($)
{
	$.fn.orlmy = function(options)
	{
		var defaults = {socket: null}
		var options =  $.extend(defaults, options);	

		var cls = null;
	    this.each(function(e)
	    { 	    	
	      //initialize the class
	      cls = new j(options.socket);
	    });
	    return cls;
	}

	//set up the class
var j = (function()
    {
      function j(s)
      {
          	var a = new App();
          	a.initAuths(s);
          	a.initXtures();
          	a.initCom(s);
          	
      }

      
    //return class object
    return j;

  }());

	//controls the layout
	function App()
	{
		//app control
		this.initXtures = function()
		{
			new Xturs().init();
		}
		this.initCom = function(socket){
			var com = new Com();
			com.comEvt(socket);
			this.uiData(socket, com);
		}
		this.initAuths = function(socket){
			new Auths().init(socket);
		}
		this.uiData = function(s, com)
		{
			var cookie_string = $.userdata($.key);
			if(typeof(cookie_string)!='undefined'){
				var c = cookie_string.res, user = $(".current-user");
				//set user name
				user.find("h4").html(c.name);
				//store user id in cookie
				$.uucode = c.uucode;
				//set the user id 
				user.attr("id",c.uucode);
			}			
			//this send unique static id associated 
			//to this user to the server
			com.onConnected(s);
			com.onDMMessageReceived(s);
		}

	}

	function Com(){
		//socket io control
		this.comEvt = function(socket){

			var s = socket;
			this.onMessageReceived(s);			
			this.onContactsReceived(s);
			this.onMessegeSend(s);
			//blur and focus
			this.onTypeFocus(s);
			this.onTpypeBlur(s);			
			//status watch
			this.onStatusChanged(s);
			this.onConversationReceived(s);

			this.onCall(s);
			
		}

		this.onCall = function(socket)
		{
			$(".fa-bell-o").click(function()
			{
				var data = {from:'+447480487479', to:'+447340960948'}
				socket.emit('call', data);
			});
		}

		this.onMessageReceived = function(socket)
		{
			$this = this;
			socket.on('message', function(msg){
				$this.row_chat_b(msg);
				//this send back message received status to sender
				$this.onMessageDelivered(socket);
			});
		}
		
		this.onConversationReceived = function(socket)
		{
			socket.on("chat_history", function(msg){
				console.log(msg);
			});
		}
		this.onDMMessageReceived = function(socket)
		{
			$this = this;
			//var useruuid = $(".current-user").attr("id");
			
			var uniqueid = $.uniqueid();

			socket.on(uniqueid, function(msg){

					//check if message is for active user
					//else check the contact list
					//then send notification
					var useruuid = $(".active-user-conversation").attr("id");
					var senderid, targetid, message = null;
					senderid = msg.uucode_sender;
					targetid = msg.uucode_target;
					message  = msg.data;
					if(useruuid == senderid){						
						$this.row_chat_b(msg.data);
						$this.onMessageDelivered(socket);
					}
					//search contact list for matching ID
					var r_text = msg.data;
					$(".conversation-list").find("li#"+senderid).find(".status-msg-update").html(r_text);
				
			});
		}

		this.onContactsReceived = function(socket)
		{
			var $this = this;
			socket.on('contacts', function(msg){
				$this.logContacts(msg);
			});
		}

		this.logContacts = function(data){
			
			var html = "";

            for(e in data){
            	html  += '<li id="'+data[e].uucode+'" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action">'+
                '<div class="media">'+
                    '<img class="d-flex mr-3 rounded-circle" src="assets/images/chat/user-3.png" alt="">'+
                        '<div class="media-body chat-media-body">'+
                            '<h5>'+data[e].fname+" "+data[e].mname+" "+data[e].lname+'</h5>'+
                        '<div class="text-muted smaller status-msg-update">Thank you for letting me..</div>'+
                    '</div>'+
                '</div>'+
                    '<span class="badge pull-right">'+
                        '3:40am<br />'+
                    	'<span class="badge badge-light">4</span>'+
                    '</span>'+
            '</li>';
            }

            $(".conversation-list").html(html);
		}

		this.onMessegeSend = function(socket){
			$this = this;
			$('form').submit(function(){
                  var msg = $('#orlmy-msg').val(), user_r = $(".active-user-conversation");
                  if(msg != ""){  
                  		var uniqueid = $.uniqueid();
                  		var data = {"data":msg, "uucode_sender":uniqueid, "uucode_target":user_r.attr("id")}                		
                  		socket.emit('dm_message', data);
						$this.row_chat_a(msg);                  		
              	  }
                  $('#orlmy-msg').val('');
                  
                  return false;
            });
		}
		

		this.onStatusChanged = function(socket){
			var typing = $(".istyping");
			socket.on("status", function(msg){
				switch(msg.id){
					case 0: typing.hide(); break;//end typing
					case 1: typing.show(); break;//start typing
					case 2:  break;//msg delivered
					case 3:  break;//msg seen/received
				}
				//control on typing, end typing, online, offline
			})
		}

		this.onTypeFocus = function(socket){
			//on input field active
			$("#orlmy-msg").on("focus", function(){
				socket.emit('status', {id:1});
			});
		}
		this.onTpypeBlur =  function(socket){
			//code for on input field blur
			$("#orlmy-msg").blur(function(){
				socket.emit('status', {id:0});
			})
		}
		this.onMessageSeen = function(socket){
			//code for message seen
			//
		}
		this.onMessageDelivered = function(socket){
			//code for message delivered
		}

		this.onConnected = function(socket){
			var uniqueid = $.uniqueid();
			socket.emit('uniqueid', uniqueid);
		}



		this.row_chat_a = function(data){
      			var html = '<div class="chat-row row-right">'+
                            '<div class="media media-right">'+
                                '<img class="d-flex mr-3 rounded-circle" src="assets/images/chat/user-2.png" alt="">'+
                             '</div>'+
                                    '<div class="popover bs-popover-left fixed-right" x-placement="left">'+
                                        '<div class="arrow" style="top: 0px;"></div>'+
                                        '<h3 class="popover-header"></h3>'+
                                        '<div class="popover-body">'+
                                          //chat message
                                         ''+data+''+
                                        '</div>'+
                                    '</div>'+                               
                                  '<br clear="both" />'+
                              '</div>';
                  $(".chat-stream-wrap").append(html);

            $.scrollStream();
    	}

    	this.row_chat_b = function(data){
      			var html = '<div class="chat-row row-left">'+
                                    '<div class="media media-left">'+
                                        '<img class="d-flex mr-3 rounded-circle" src="assets/images/chat/user-1.png" alt="">'+
                                    '</div>'+                                    
                                    '<div class="popover bs-popover-right fixed-left" x-placement="left">'+
                                        '<div class="arrow" style="top: 0px;"></div>'+
                                        '<h3 class="popover-header"></h3>'+
                                        '<div class="popover-body">'+
                                        //chat message
                                        ''+data+''+
                                        '</div>'+
                                    '</div>'+
                                    '<br clear="both" />'+
                              '</div>';
                        $(".chat-stream-wrap").append(html);

                $.scrollStream();
    	}

	}

	function Xturs(){
			//controls the app structures
		this.init = function(){	

			this.uiControl();
			this.setOrlmyRow();
			this.onWindowResize();
			this.switchChart();

		}
		this.setOrlmyRow = function()
		{
			var h = this.height;
			$(".app-fluid").css({height:h+"px"});
            $(".orlmy-row-1,.orlmy-row-2,.orlmy-row-3").css({height:h+"px"});
			//$(".orlmy-row-index").css({height:h+"px"});
			//console.log(h)
		}
		this.width = null;
		this.height = null;
		this.uiControl = function()
		{
			this.width = this.getWidth($(window));
			this.height = this.getHeight($(window));
			//the rest of layout control
			
			//ui for chat stream
			this.setMobileStream();
			//controll all height:
		}
		this.switchChart = function(){//control who is chating

			var $this = this;
			$("body").on("click", ".chat-media-body", function(){
				var id = $(this).parents("li").attr("id");				
				//ui flow
				var row1 = $(".orlmy-row-1"), row2 =  $(".orlmy-row-2"), row3 = $(".orlmy-row-3");
				if($this.width <= 480){//serve mobile
					row2.css({zIndex:1});					
				}//@endif

				//clear chat stream and request user messages
				$(".chat-stream-wrap").html("");
				//request chat history
				$this.setUserForActiveChat($(this));
			});
		}
		this.setUserForActiveChat = function(e){
			var uuid = e.parents("li").attr("id"), name = e.find("h5").text(), cnt = $(".active-user-conversation");
			cnt.find("h4").html(name); cnt.attr("id", uuid);
			//get chat history				
			socket.emit('chat_history', uuid);
		}
		this.headerHeight = 80;
		this.inputHeight = 90;
		this.setMobileStream = function(){
			var cStream = this.height - (this.headerHeight+this.inputHeight);
			$(".main-stream-wrap").css({height:cStream+"px"});
			$(".orlmy-row-2").removeClass("chat-stream").css({height:this.height+"px"});

			//adjust all row 
			this.setOrlmyRow();

		}
		this.onWindowResize = function(){
			var $this = this;
			$(window).resize(function(){
				$this.uiControl();
			});
		}
		this.getWidth = function(el){
			return el.width()
		}
		this.getHeight = function(el){
			return el.height();
		}
	}

	function Auths(){
		//resources such as files, images etc
		this.init = function(s)
		{
			this.login(s);
			this.signup(s);
			this.onServerAuthsReceived(s);
		}
		this.login = function(socket){
			var $this = this;
			$(".ln").click(function(){
				var data = $this.fgetv("#login", ".fn");
				socket.emit('auths', data);
			});
		}
		this.onServerAuthsReceived = function(socket){
			var $this = this;
			socket.on("auths", function(msg){				
				var res = "";
				if(msg.login == 1){
					res = "Authentication successful!";
					$.set_userdata($.key, msg);
					document.location.href = "app.html";
				}else{
					res = "Invalid username or password";
				} 		
				alert(res)		
				$(".s-response").html(res);
			})
		}
		this.signup = function(socket){
			$("#signup").click(function(){

			});
		}
		
		this.fgetv = function(formid, key, name)
		{
			var d = {}, f = $(formid).find(key);
			f.each(function()
			{
				var k = $(this).attr("name"), v = $(this).val();
				
				if($(this).val()!="" && !$(this).hasClass("ignore"))
				{
						d[k] = v;
				}
			});
			return d;

		}
	}

})(jQuery)

jQuery.extend({
	uniqueid:function(){
	  	return $.uucode;
	},
	key:"urtehryfr",
	uucode:null,
	userdata:function(key)
	{
		var session = $.cookie(key);
		if(typeof(session) != 'undefined'){
			var data =  $.parseJSON(session);
			if(!data){ return ""; }else{ return data; }	
		}				
	},
	set_userdata:function(key, data)
	{
		$.cookie(key, JSON.stringify(data));
	},
	unset_userdata:function(key)
	{
		$.cookie(key, null);
	},
	randomid:function(){
		return Math.random().toString(36).split('').filter( function(value, index, self) { 
        return self.indexOf(value) === index;
    	}).join('').substr(2,8);
	},
	scrollStream:function(){

		var chatStreamWrap = $(".chat-stream-wrap"), chatStreamWrapHeight = chatStreamWrap.height();
		var mainStreamWrap = $(".main-stream-wrap"), mainChatStreamHeight = mainStreamWrap.height();
		//alert(mainChatStreamHeight+"|"+chatStreamWrapHeight)
		if(mainChatStreamHeight <= chatStreamWrapHeight)
		{
			$('.main-stream-wrap').stop().animate({
    			'scrollTop' : chatStreamWrapHeight
			}, 100);
		}
	}
})

 $(function()
 { 
    $("body").orlmy({socket:io()});
});