

function Drawer(){
	this.init = function(){

		this.drawerOpen();
		this.drawerClose();
	}
	this.drawer = $(".drawer");
	this.drawerOpen = function(){

		var $this = this;
		$(".showDrawer").click(function(){
			$this.drawer.animate({right: "0"});
		});
	}
	this.drawerClose = function(){
		var $this = this;
		$(".exit").click(function(){
			$this.drawer.animate({right: "-600px"});
		});
	}
}
$(function(){

	/*
	* Initialize the pseudo class drawer */
	new Drawer().init();
})